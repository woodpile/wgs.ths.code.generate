package main

import (
	"context"
	"fmt"

	"gitee.com/woodpile/wgs.ths.code.generate/definition"
	"github.com/urfave/cli/v3"
	"gopkg.in/yaml.v3"
)

func init() {
	subCmd = append(subCmd, cmdConfig)
}

var cmdConfig = &cli.Command{
	Name:   "config",
	Usage:  `config tools`,
	Action: actionCmdConfig,
}

func actionCmdConfig(ctx context.Context, cmd *cli.Command) error {
	if err := loadConfig(cmd); err != nil {
		return err
	}

	dc := newContext()
	if err := loadScript(dc); err != nil {
		return err
	}

	printGlobalConfig()
	return nil
}

func printGlobalConfig() {
	bs, err := yaml.Marshal(definition.GlobalConfig)
	if err != nil {
		panic(err)
	}
	fmt.Printf("------------\n%s\n", string(bs))
}
