package main

import (
	"context"

	"gitee.com/woodpile/wgs.ths.code.generate/definition"
	"github.com/urfave/cli/v3"
)

func init() {
	subCmd = append(subCmd, cmdGenerate)
}

var cmdGenerate = &cli.Command{
	Name:   "generate",
	Usage:  "generate code file",
	Action: actionCmdGenerate,
}

func actionCmdGenerate(ctx context.Context, cmd *cli.Command) error {
	if err := loadConfig(cmd); err != nil {
		return err
	}
	if definition.GlobalConfig.Verbose {
		printGlobalConfig()
	}

	dc := newContext()
	if err := loadScript(dc); err != nil {
		return err
	}
	if err := definition.DoParse(dc); err != nil {
		return err
	}
	if err := definition.LoadTemplate(dc); err != nil {
		return err
	}
	if err := definition.DoGenerate(dc); err != nil {
		return err
	}

	return nil
}
