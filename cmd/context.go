package main

import (
	"gitee.com/woodpile/wgs.ths.code.generate/definition"
)

func newContext() *definition.Context {
	ctx := definition.NewContext()
	return ctx
}

func loadScript(ctx *definition.Context) error {
	if err := definition.LoadScript(ctx); err != nil {
		return err
	}

	if err := scriptModifyConfig(ctx); err != nil {
		return err
	}

	return nil
}
