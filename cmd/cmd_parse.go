package main

import (
	"context"

	"gitee.com/woodpile/wgs.ths.code.generate/definition"
	"github.com/urfave/cli/v3"
)

func init() {
	subCmd = append(subCmd, cmdParse)
}

var cmdParse = &cli.Command{
	Name:   "parse",
	Usage:  "parse source files",
	Action: actionCmdParse,
}

func actionCmdParse(ctx context.Context, cmd *cli.Command) error {
	if err := loadConfig(cmd); err != nil {
		return err
	}
	if definition.GlobalConfig.Verbose {
		printGlobalConfig()
	}

	dc := newContext()
	if err := loadScript(dc); err != nil {
		return err
	}
	if err := definition.DoParse(dc); err != nil {
		return err
	}

	return nil
}
