package main

import (
	"context"
	"fmt"
	"os"

	"github.com/urfave/cli/v3"

	"gitee.com/woodpile/wgs.ths.code.generate/cmd/version"
)

var subCmd = []*cli.Command{}

func main() {
	app := cli.Command{
		Name:     "wgs_code_generate",
		Version:  version.Version,
		Usage:    "code generate toolkit for wgs projects",
		Commands: subCmd,
		Flags: []cli.Flag{
			&cli.StringFlag{
				Name:    "config",
				Aliases: []string{"c"},
				Value:   "config.yaml",
				Usage:   "config file path",
			},
			&cli.BoolFlag{
				Name:  "verbose",
				Usage: "verbose mode",
			},
		},
		Action: nil,
	}

	if err := app.Run(context.Background(), os.Args); err != nil {
		fmt.Printf("Error: %v\n", err)
		os.Exit(1)
	}
}
