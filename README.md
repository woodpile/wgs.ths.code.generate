# wgs.ths.code.generate

## 介绍

go语言实现的一个结构性代码生成工具, 适用于生成协议配套代码, 序列化套件代码等.

## 安装

```shell
go get gitee.com/woodpile/wgs.ths.code.generate
go build -o ths_code_generate gitee.com/woodpile/wgs.ths.code.generate/cmd
```

## 使用说明

### 必须使用配置文件

```shell
./ths_code_generate -c config.yaml
```

### 基于子命令执行程序

```text
COMMANDS:
   config    config tools
   generate  generate code file
   parse     parse source files
   help, h   Shows a list of commands or help for one command
```
