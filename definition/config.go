package definition

import (
	config "gitee.com/woodpile/wgs.ths.config.go"
)

func init() {
	config.DefaultContext.RegisterRootConfig(GlobalConfig)
}

type Config struct {
	Verbose bool        `yaml:"verbose,omitempty"`
	Cmd     interface{} `yaml:"cmd,omitempty"`

	Parser    *ParserConfig    `yaml:"parser,omitempty"`
	Generator *GeneratorConfig `yaml:"generator,omitempty"`
	Template  *TemplateConfig  `yaml:"template,omitempty"`
	Script    *ScriptConfig    `yaml:"script,omitempty"`
}

var GlobalConfig = &Config{
	Verbose: false,
}

func (*Config) Name() string {
	return "root"
}

func (*Config) ToDefault(cfg config.IConfigDefault) {
	ac := cfg.(*Config)
	ac.Verbose = false
	ac.Parser = &ParserConfig{}
	ac.Generator = &GeneratorConfig{}
}
