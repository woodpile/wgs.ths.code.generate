package definition

type Context struct {
	DFSet *DFSet

	Template *TemplateManager

	FuncConfigModify []FConfigModify
	FuncParsePre     []FParsePre
	FuncParsePost    []FParsePost
	FuncGeneratePre  []FGeneratePre
	FuncGeneratePost []FGeneratePost
	FuncTemplateFunc []FTemplateFunc
}

func NewContext() *Context {
	ctx := &Context{
		DFSet: NewDFSet(),

		Template: NewTemplateManager(),
	}
	return ctx
}

func (ctx *Context) AddDefinedFile(df *DFFile) {
	if ctx.DFSet.mFileAsPath[df.Path] == nil {
		ctx.DFSet.Files = append(ctx.DFSet.Files, df)
		ctx.DFSet.mFileAsPath[df.Path] = df
	}

	for _, de := range df.Enums {
		ctx.DFSet.mEnums[de.Name] = de
	}
	for _, ds := range df.Structs {
		ctx.DFSet.mStructs[ds.Name] = ds
	}
}
