package definition

type DFSet struct {
	Files []*DFFile

	mFileAsPath map[string]*DFFile
	mEnums      map[string]*DFEnum
	mStructs    map[string]*DFStruct
}

func NewDFSet() *DFSet {
	return &DFSet{
		Files: make([]*DFFile, 0, 64),

		mFileAsPath: make(map[string]*DFFile),
		mEnums:      make(map[string]*DFEnum),
		mStructs:    make(map[string]*DFStruct),
	}
}

func (set *DFSet) GetFileByPath(path string) *DFFile {
	return set.mFileAsPath[path]
}

type DFFile struct {
	Name    string
	Path    string
	Options map[string]string

	Enums   []*DFEnum
	Structs []*DFStruct

	Comment string
}

type DFEnum struct {
	Name       string
	SourceFile string
	Options    map[string]string
	Values     []*DFEnumValue

	Comment string
}

type DFEnumValue struct {
	Name    string
	Options map[string]string
	Value   string

	Comment string
}

type DFStruct struct {
	Name       string
	SourceFile string
	Options    map[string]string

	Fields []*DFStructField
	OneOfs []*DFStructOneOf

	Comment string
}

func (ds *DFStruct) GetField(name string) *DFStructField {
	for _, f := range ds.Fields {
		if f.Name == name {
			return f
		}
	}
	return nil
}

func (ds *DFStruct) GetOneOf(name string) *DFStructOneOf {
	for _, f := range ds.OneOfs {
		if f.Name == name {
			return f
		}
	}
	return nil
}

type DFStructOneOf struct {
	Name    string
	Options map[string]string
	Fields  []*DFStructField

	Comment string
}

type DFStructField struct {
	Name    string
	Options map[string]string

	FieldType DFFieldType

	Comment string
}

type DFFieldType struct {
	ValueType DFType
	KeyType   *DFType

	IsArray bool
	IsMap   bool
}

type DFType struct {
	Source string

	IsEnum   bool
	IsStruct bool
}
