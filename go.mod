module gitee.com/woodpile/wgs.ths.code.generate

go 1.19

require (
	gitee.com/woodpile/wgs.ths.config.go v0.1.3
	github.com/jhump/protoreflect v1.15.3
	github.com/traefik/yaegi v0.15.1
	github.com/urfave/cli/v3 v3.0.0-alpha7
	google.golang.org/protobuf v1.31.0
	gopkg.in/yaml.v3 v3.0.1
)

require (
	github.com/bufbuild/protocompile v0.6.0 // indirect
	github.com/golang/protobuf v1.5.3 // indirect
	github.com/xrash/smetrics v0.0.0-20201216005158-039620a65673 // indirect
	golang.org/x/sync v0.3.0 // indirect
)
