P_VERSION=$1
if [ -z "$P_VERSION" ]
then
    DATESTR=`date "+%Y%m%d%H%M%S"`
    P_VERSION="0.0.0.${DATESTR}"
    echo "no version param. generate: ${P_VERSION}"
fi

P_EXEFILE="ths_code_generator"

if [ "${OSTYPE}" == "msys" ]
then
    P_EXEFILE="${P_EXEFILE}.exe"
fi

if [ -e "./$P_EXEFILE" ]
then
    rm ./${P_EXEFILE}
fi

go build -o ${P_EXEFILE} -ldflags "-X gitee.com/woodpile/wgs.ths.code.generate/cmd/version.Version=${P_VERSION}" gitee.com/woodpile/wgs.ths.code.generate/cmd