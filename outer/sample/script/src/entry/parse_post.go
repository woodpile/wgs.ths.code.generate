package entry

import (
	"fmt"

	"gitee.com/woodpile/wgs.ths.code.generate/definition"
)

func ParsePostAddMessageFieldOption(ctx *definition.Context) error {
	// fmt.Printf("ParsePreAddMessageField: %+v\n", ctx)

	var st *definition.DFStruct
	for _, vf := range ctx.DFSet.Files {
		for _, vst := range vf.Structs {
			if vst.Name == "MovableJumpVertical" {
				st = vst
				break
			}
		}
	}
	if st == nil {
		return fmt.Errorf("not found struct MovableJumpVertical")
	}
	var stf *definition.DFStructField
	for _, v := range st.Fields {
		if v.Name == "height" {
			stf = v
			break
		}
	}
	if stf == nil {
		return fmt.Errorf("not found struct field height")
	}

	stf.Options["ths_Min"] = "4"

	return nil
}
