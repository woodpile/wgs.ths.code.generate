package entry

import (
	"strings"
	"text/template"
)

func TemplateFunc() template.FuncMap {
	return template.FuncMap{
		"FiledNameToGo": func(name string) string {
			if len(name) == 0 {
				return name
			}
			return strings.ToUpper(name[0:1]) + name[1:]
		},
	}
}
