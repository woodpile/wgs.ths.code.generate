package entry

import (
	"gitee.com/woodpile/wgs.ths.code.generate/definition"
)

func Load() *definition.ScriptPackage {
	var sp = &definition.ScriptPackage{
		ConfigModifyFunc: []definition.FConfigModify{
			ConfigModify0,
		},

		ParsePostFunc: []definition.FParsePost{
			ParsePostAddMessageFieldOption,
		},

		TemplateFunc: []definition.FTemplateFunc{
			TemplateFunc,
		},
	}
	return sp
}
