#!/bin/bash

# set -e

DIR=`dirname $0`
cd ${DIR}

export PATH=$PATH:./bin:./protoc/bin

protoc -I=./src --go_opt=module=gitee.com/woodpile/wgs.ths.code.generate/outer/sample/dst --go_out=./dst ./src/common/*.proto
protoc -I=./src --go_opt=module=gitee.com/woodpile/wgs.ths.code.generate/outer/sample/dst --go_out=./dst ./src/proto/*.proto