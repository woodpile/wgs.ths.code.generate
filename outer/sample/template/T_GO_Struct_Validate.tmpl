{{define "T_FIELD_CHECK"}}
{{- if .Options.ths_Min}}
    if v.{{FiledNameToGo .Name}} < {{.Options.ths_Min}} {
        return false, "{{.Name}} cannot be less than {{.Options.ths_Min}}"
    }
{{end}}
{{- if .Options.ths_Max}}
    if v.{{FiledNameToGo .Name}} > {{.Options.ths_Max}} {
        return false, "{{.Name}} cannot be bigger than {{.Options.ths_Max}}"
    }
{{end}}
{{- if .Options.ths_MinLen}}
    if len(v.{{FiledNameToGo .Name}}) < {{.Options.ths_MinLen}} {
        return false, "{{.Name}} length cannot be less than {{.Options.ths_MinLen}}"
    }
{{end}}
{{- if .Options.ths_MaxLen}}
    if len(v.{{FiledNameToGo .Name}}) > {{.Options.ths_MaxLen}} {
        return false, "{{.Name}} length cannot be bigger than {{.Options.ths_MaxLen}}"
    }
{{end}}
{{- if .Options.ths_Custom}}
    if !{{.Options.ths_Custom}}(v.{{FiledNameToGo .Name}}) {
        return false, "{{.Name}} is not valid as {{.Options.ths_Custom}}"
    }
{{end}}
{{- end}}

{{define "T_ARRAY_FIELD_CHECK"}}
{{- if .Options.ths_MinLen}}
    if len(v.{{FiledNameToGo .Name}}) < {{.Options.ths_MinLen}} {
        return false, "{{.Name}} length cannot be less than {{.Options.ths_MinLen}}"
    }
{{end}}
{{- if .Options.ths_MaxLen}}
    if len(v.{{FiledNameToGo .Name}}) > {{.Options.ths_MaxLen}} {
        return false, "{{.Name}} length cannot be bigger than {{.Options.ths_MaxLen}}"
    }
{{end}}
{{- if .Options.ths_Custom}}
    if !{{.Options.ths_Custom}}(v.{{FiledNameToGo .Name}}) {
        return false, "{{.Name}} is not valid as {{.Options.ths_Custom}}"
    }
{{end}}
{{- end}}

{{define "T_MAP_FIELD_CHECK"}}
{{- if .Options.ths_MinLen}}
    if len(v.{{FiledNameToGo .Name}}) < {{.Options.ths_MinLen}} {
        return false, "{{.Name}} length cannot be less than {{.Options.ths_MinLen}}"
    }
{{end}}
{{- if .Options.ths_MaxLen}}
    if len(v.{{FiledNameToGo .Name}}) > {{.Options.ths_MaxLen}} {
        return false, "{{.Name}} length cannot be bigger than {{.Options.ths_MaxLen}}"
    }
{{end}}
{{- if .Options.ths_Custom}}
    if !{{.Options.ths_Custom}}(v.{{FiledNameToGo .Name}}) {
        return false, "{{.Name}} is not valid as {{.Options.ths_Custom}}"
    }
{{end}}
{{- end}}

{{define "T_ONEOF_CHECK"}}
{{ $ONEOF_NAME := .Name }}
{{- if .Options.ths_Required}}
    if v.Get{{FiledNameToGo .Name}}() == nil {
        return false, "{{FiledNameToGo .Name}} is required"
    }
{{end}}
{{- range .Fields}}
{{- if .FieldType.IsArray}}    {{template "T_ARRAY_FIELD_CHECK" .}}
{{else if .FieldType.IsMap}}    {{template "T_MAP_FIELD_CHECK" .}}
{{else}}
    if sub := v.Get{{FiledNameToGo .Name}}(); sub != nil {
        if ok, err := sub.Validate(); !ok {
            return false, "{{FiledNameToGo $ONEOF_NAME}}.{{FiledNameToGo .Name}}" + err
        }
    }
{{end}}
{{end}}
{{- end}}

func (v *{{.Name}}) Validate() (bool, string) {
{{- range .Fields}}
{{- if .FieldType.IsArray}}    {{template "T_ARRAY_FIELD_CHECK" .}}
{{else if .FieldType.IsMap}}    {{template "T_MAP_FIELD_CHECK" .}}
{{else}}{{template "T_FIELD_CHECK" .}}
{{end}}
{{end}}
{{- range .OneOfs}}
{{template "T_ONEOF_CHECK" .}}
{{end}}
    return true, ""
}