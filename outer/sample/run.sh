EXE_PATH="./ths_cg.out"

if [ -f $EXE_PATH ]; then
    # echo "rm ${EXE_PATH}"
    rm $EXE_PATH
fi

cd ../../cmd
go build -o $EXE_PATH
mv $EXE_PATH ../outer/sample
cd ../outer/sample

# Run
# ./ths_cg.out "$@"
./ths_cg.out -c ./sample.config.ths_cg.yaml "$@"